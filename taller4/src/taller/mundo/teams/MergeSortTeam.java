package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          return merge_sort(lista, orden);
     }

     // Implementado de http://howtodoinjava.com/algorithm/merge-sort-java-example/

     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 // Trabajo en Clase
    	 if(lista.length<= 1)
    	 {
    		 return lista;
    	 }
    	 Comparable[] izquierda = new Comparable[lista.length/2];
    	 Comparable[] derecha = new Comparable[lista.length - izquierda.length];
    	 for(int i = 0; i < izquierda.length; i++)
    	 {
    		 izquierda[i] = lista[i];
    	 }
    	 for(int j = 0; j < derecha.length; j++)
    	 {
    		 for(int y = izquierda.length; y < lista.length; y++)
    		 {
    			 derecha[j] = lista[y];
    		 }
    	 }
    	 
    	 merge_sort(izquierda, orden);
    	 merge_sort(derecha, orden);
    	 
    	 merge(izquierda, derecha, orden);
    	 
    	 return lista;
     }

     private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
     {
    	// Trabajo en Clase 
    	 Comparable[] arr = new Comparable[izquierda.length+derecha.length];
    	 int indIzq = 0;
    	 int indDer = 0;
    	 int merge = 0;
    	 while(indIzq < izquierda.length && indDer < derecha.length)
		 {
    		 if(orden == TipoOrdenamiento.ASCENDENTE)
        	 {
    			 if(izquierda[indIzq].compareTo(derecha[indDer])< 0)
    			 {
    				 arr[merge] = izquierda[indIzq];
    				 indIzq++;
    			 }
    			 else
    			 {
    				 arr[merge] = derecha[indDer];
    				 indDer++;
    			 }
        	 }
    		 else if (orden == TipoOrdenamiento.DESCENDENTE)
    		 {
    			 if(izquierda[indIzq].compareTo(derecha[indDer])> 0)
    			 {
    				 arr[merge] = izquierda[indIzq];
    				 indIzq++;
    			 }
    			 else
    			 {
    				 arr[merge] = derecha[indDer];
    				 indDer++;
    			 }
    		 }
			 
			 merge++;
		 }
    	 
    	 System.arraycopy(izquierda, indIzq, arr, merge, izquierda.length - indIzq);
    	 System.arraycopy(derecha, indDer, arr, merge, derecha.length - indDer);

    	
    	 return arr;
     }


}
