package taller.mundo.teams;

import java.util.Arrays;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class HybridSortTeam extends AlgorithmTeam
{

	public HybridSortTeam() {
		super("Hybrid sort (-)");
		userDefined = false;
		
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden) {
		// TODO Auto-generated method stub
		
		return hybridSort(list, orden);
	}
	
	public Comparable[] hybridSort(Comparable[] list, TipoOrdenamiento orden)
	{
		Comparable[] primero = new Comparable[list.length/2];
		System.arraycopy(list, 0, primero, 0, list.length/2);
		Comparable[] segundo = new Comparable[list.length-primero.length];
		System.arraycopy(list, list.length - primero.length, segundo , 0, list.length - primero.length);
		Comparable[] arr = new Comparable[list.length];
		
		bubbleSort(primero, orden);
		insertionSort(segundo, orden);
		System.arraycopy(primero, 0, arr, 0, primero.length);
		System.arraycopy(segundo, 0, arr, arr.length - primero.length, segundo.length);
		
		return timSort(arr, orden);
	}
	
	public Comparable[] bubbleSort(Comparable[] list, TipoOrdenamiento orden)
	{
		Comparable elemento;
   	 for(int i = 0; i < list.length-1; i++)
   	 {
   		 for(int j = 1; j< list.length-1; j++)
   		 {
   			 if(orden == TipoOrdenamiento.ASCENDENTE)
   			 {
   				 if(list[j-1].compareTo(list[j]) > 0 )
   				 {
   					 elemento = list[j-1];
   					 list[j-1] = list[j];
   					 list[j] = elemento;
   				 }
   			 }
   			 else if(orden == TipoOrdenamiento.DESCENDENTE)
   			 {
   				 if(list[j-1].compareTo(list[j]) < 0 )
   				 {
   					 elemento = list[j-1];
   					 list[j-1] = list[j];
   					 list[j] = elemento;
   				 }
   			 }
   		 }
   	 }
   	 
   	 return list;
    }
	
	
	public Comparable[] insertionSort(Comparable[] list, TipoOrdenamiento orden)
	{
		for(int i = 1; i < list.length;i++)
		{
			Comparable elemento = list[i];
			int j;
			if(orden == TipoOrdenamiento.DESCENDENTE)
			{
				for (j = i-1; j>=0 && elemento.compareTo(list[j])<0;j--)
				{
					list[j+1] = list[j];

				}  
				list[j+1] = elemento;
			}
			else if (orden == TipoOrdenamiento.ASCENDENTE)
			{
				for (j = i-1; j>=0 && elemento.compareTo(list[j])>0;j--)
				{
					list[j+1] = list[j];

				}  
				list[j+1] = elemento;
			}

		}

		return list;
	}
	
	public Comparable[] timSort(Comparable[] list, TipoOrdenamiento orden)
    {
          Arrays.sort(list);
          if(orden == TipoOrdenamiento.DESCENDENTE)
          {
               return reverseArray(list);
          }
          return list;
    }

    public Comparable[] reverseArray(Comparable[] list)
    {
         for(int i = 0; i < list.length/2; i++)
         {
             Comparable temp = list[i];
             list[i] = list[list.length - 1 - i];
             list[list.length - 1 - i] = temp; 
         }
         return list;
   }

	

}
