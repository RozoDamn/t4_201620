package taller.test;

import java.sql.Array;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.BubbleSortTeam;
import taller.mundo.teams.HybridSortTeam;
import taller.mundo.teams.InsertionSortTeam;
import taller.mundo.teams.MergeSortTeam;
import taller.mundo.teams.QuickSortTeam;
import taller.mundo.teams.SelectionSortTeam;
import junit.framework.TestCase;

public class MainTest extends TestCase{

	private BubbleSortTeam burbuja; 
	private InsertionSortTeam insercion;
	private SelectionSortTeam seleccion;
	private QuickSortTeam quick;
	private MergeSortTeam merge;
	private HybridSortTeam hybrid;

	public void setupScenario1()
	{
		burbuja = new BubbleSortTeam();

	}

	public void setupScenario2()
	{
		insercion = new InsertionSortTeam();
	}

	public void setupScenario3()
	{
		seleccion = new SelectionSortTeam();
	}
	
	public void setupScenario4()
	{
		quick = new QuickSortTeam();
	}
	
	public void setupScenario5()
	{
		merge = new MergeSortTeam();
	}
	
	public void setupScenario6()
	{
		hybrid = new HybridSortTeam();
	}

	public void testBurbuja()
	{
		setupScenario1();
		Comparable[] arr = new Comparable[10];
		for(int i = 0; i < arr.length; i++)
		{
			arr[i] = (int)Math.random();
		}

		//Caso1: Se ordena la lista de manera descendente
		Comparable[] resp = burbuja.sort(arr, TipoOrdenamiento.DESCENDENTE);
		for(int j = 0; j< resp.length -1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == 1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}

		//Caso2: Se ordena la lista de manera ascendente
		resp = burbuja.sort(arr, TipoOrdenamiento.ASCENDENTE);
		for(int j = 0; j< resp.length-1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == -1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}


	}
	
	public void testInsercion()
	{
		setupScenario2();
		Comparable[] arr = new Comparable[10];
		for(int i = 0; i < arr.length; i++)
		{
			arr[i] = (int)Math.random();
		}

		//Caso1: Se ordena la lista de manera descendente
		Comparable[] resp = insercion.sort(arr, TipoOrdenamiento.DESCENDENTE);
		for(int j = 0; j< resp.length -1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == 1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}

		//Caso2: Se ordena la lista de manera ascendente
		resp = insercion.sort(arr, TipoOrdenamiento.ASCENDENTE);
		for(int j = 0; j< resp.length-1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == -1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}

	}
	public void testSeleccion()
	{
		setupScenario3();
		Comparable[] arr = new Comparable[10];
		for(int i = 0; i < arr.length; i++)
		{
			arr[i] = (int)Math.random();
		}

		//Caso1: Se ordena la lista de manera descendente
		Comparable[] resp = seleccion.sort(arr, TipoOrdenamiento.DESCENDENTE);
		for(int j = 0; j< resp.length -1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == 1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}

		//Caso2: Se ordena la lista de manera ascendente
		resp = seleccion.sort(arr, TipoOrdenamiento.ASCENDENTE);
		for(int j = 0; j< resp.length-1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == -1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}

	}
	
	public void testQuickSort()
	{
		setupScenario4();
		Comparable[] arr = new Comparable[10];
		for(int i = 0; i < arr.length; i++)
		{
			arr[i] = (int)Math.random();
		}

		//Caso1: Se ordena la lista de manera descendente
		Comparable[] resp = quick.sort(arr, TipoOrdenamiento.DESCENDENTE);
		for(int j = 0; j< resp.length -1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == 1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}

		//Caso2: Se ordena la lista de manera ascendente
		resp = quick.sort(arr, TipoOrdenamiento.ASCENDENTE);
		for(int j = 0; j< resp.length-1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == -1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}
	}
	
	public void testMergeSort()
	{
		setupScenario5();
		Comparable[] arr = new Comparable[10];
		for(int i = 0; i < arr.length; i++)
		{
			arr[i] = (int)Math.random();
		}

		//Caso1: Se ordena la lista de manera descendente
		Comparable[] resp = merge.sort(arr, TipoOrdenamiento.DESCENDENTE);
		for(int j = 0; j< resp.length -1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == 1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}

		//Caso2: Se ordena la lista de manera ascendente
		resp = merge.sort(arr, TipoOrdenamiento.ASCENDENTE);
		for(int j = 0; j< resp.length-1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == -1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}
	}
	
	public void testHybridSort()
	{
		setupScenario6();
		Comparable[] arr = new Comparable[10];
		for(int i = 0; i < arr.length; i++)
		{
			arr[i] = (int)Math.random();
		}

		//Caso1: Se ordena la lista de manera descendente
		Comparable[] resp = hybrid.sort(arr, TipoOrdenamiento.DESCENDENTE);
		for(int j = 0; j< resp.length -1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == 1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}

		//Caso2: Se ordena la lista de manera ascendente
		resp = hybrid.sort(arr, TipoOrdenamiento.ASCENDENTE);
		for(int j = 0; j< resp.length-1; j++)
		{
			Comparable ass = null;
			Comparable resp1 = resp[j];
			Comparable resp2 = resp[j+1];
			assertTrue("La lista no se esta ordenando de manera correcta.", resp1.compareTo(resp2) == -1 || resp1.compareTo(resp2) == 0);
			for(int i = 0; i < arr.length && ass == null;i++)
			{
				if(resp1.compareTo(arr[i]) == 0)
				{
					ass = arr[i];
				}
			}
			if(ass == null)
			{
				fail("Los elemento en la lista ordenada no son los mismo a los de la desordenada");
			}
		}
	}
	


	public void testSample() {
		assertEquals(2, 2);
	}
}